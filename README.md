# Encode: Currency Exchange Rates

Please create a Currency exchange rates app using the exchange rates API with currency
conversion. We expect you to use TypeScript and React’s latest version and features.
You can use create react app with TypeScript.

- We pay attention to performance, so make sure you optimize your app and eliminate
  redundant re-renders and computations.
- **Do not use** state management tools like Redux.
- You need to write unit tests to test some of your most important functions.
- Styling is not of utmost importance, however, we expect at least basic styling. You
  can use 3rd party libraries such as ~material ui, semantic-ui~. (antd chosen)

Please send us back your app as a link to a GitHub repository or as a zipped file.

## Functional Requirements:

- The application must display all currencies available in the API together with their
  conversion rates (defaults to Euro).
- Must allow the user to select a preferred currency.
- Upon currency selection, a line graph should be displayed. The graph must show the
  exchange rate for the last 7 days. You can use react-chartjs-2 or any other chart
  package.
- Upon currency selection, the application must show the max and min value for the
  exchange rate for the last 7 days along with the respective date.

## Requirements

This project requires a `REACT_APP_API_KEY` env var set up with a functioning API key from https://exchangeratesapi.io/ in a local `.env.local` file.

## Available Scripts

In the project directory, you can run:

- `npm start`
- `npm test`
- `npm run build`

## Improvements

- Split currencies between common and uncommon

## Tests

- Tests added for the three basic components: the base currency selector, the main exchange app (selector + chart) and the error message in case API fails.
- More advanced tests could be added to test hooks & transformations.

## Issues

### API

- The free API plan functionality is very limited, and many symbols (currencies) are blocked behind paywall (`Bad Request 400`):
  - Selecting a _paywalled_ symbol will break the App as you won't be able to get the list of symbols again using the `$base` again.

### Performance Optimization

- 7 requests to simulate a time-line (API limitations); no, I don't want to reuse the initial request data to fill the current day.

- Cacheable ETags for requests

- Setting the currency (header) triggers two (2) renders of ExchangeTable component: context change and useFetchExchangeRates hook.

### Security

- **Never** use an `.env` file for storing API keys as they are often bundled and you can see API key in the source code.

### Styling

- Not responsive (not even bothered to use built-in breakpoints)
