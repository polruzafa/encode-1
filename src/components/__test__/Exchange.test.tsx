import React from 'react'
import { render, screen } from '@testing-library/react'
import { Exchange } from '../Exchange'
import LATEST from '../../hooks/mocks/latest.json'

describe('Exchange component', () => {
  beforeAll(() => {
    // Official: https://jestjs.io/docs/manual-mocks#mocking-methods-which-are-not-implemented-in-jsdom
    window.matchMedia = (query) => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    })
  })

  test('renders main exchange component', () => {
    const payload: any = { ...LATEST }
    render(<Exchange payload={payload} />)
    const elementText = screen.getByText(/Select any currency/i)
    expect(elementText).toBeInTheDocument()
  })
})
