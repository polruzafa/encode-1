import React from 'react'
import { render, screen } from '@testing-library/react'
import { PreferredSelector } from '../PreferredSelector'
import CONSTANTS from '../../constants.json'

describe('Preferred (Base currency) Selector', () => {
  test('renders selector with base currencies', () => {
    const currencies: string[] = [...CONSTANTS.MOST_COMMON_SYMBOLS]
    render(<PreferredSelector currencies={currencies} />)
    const elementText = screen.getByText(/Base currency/i)
    expect(elementText).toBeInTheDocument()
  })
})
