import React from 'react'
import { render, screen } from '@testing-library/react'
import { Error } from '../Error'
import CONSTANTS from '../../constants.json'

describe('Error component', () => {
  test('renders error messages', () => {
    const e: any = {
      error: {
        code: CONSTANTS.RESTRICTED_CURRENCY_ACCESS_ERROR_MESSAGE,
        message: CONSTANTS.SOMETHING_WENT_WRONG,
      },
    }
    render(<Error e={e} />)
    const errorCode = screen.getByText(/Error code/i)
    const alertMessage = screen.getByText(/This error means/i)
    expect(errorCode).toBeInTheDocument()
    expect(alertMessage).toBeInTheDocument()
  })
})
