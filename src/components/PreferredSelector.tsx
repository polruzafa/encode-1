import React from 'react'
import { Select } from 'antd'
import { useCurrency } from '../hooks/useCurrency'

const { Option } = Select

export const PreferredSelector = ({
  currencies = [],
}: {
  currencies: string[]
}): JSX.Element => {
  const { currency, setCurrency } = useCurrency()

  const changeHandler = (value: string) => {
    if (setCurrency) setCurrency(value)
  }

  return (
    <>
      <span>Base currency</span>
      <Select
        showSearch
        defaultValue={currency}
        style={{ width: 120 }}
        onChange={changeHandler}
        size="small"
      >
        {currencies.map(
          (currency): JSX.Element => (
            <Option key={currency} value={currency}>
              {currency}
            </Option>
          )
        )}
      </Select>
    </>
  )
}
