import React from 'react'
import { Select } from 'antd'

const { Option } = Select

// Data management
interface IRate {
  key: React.Key
  currency: string
  rate: any
}

export const Selector = ({
  rates = [],
  selected,
  handler,
}: {
  rates: any
  selected: string | undefined
  handler: any
}): JSX.Element => {
  const data: IRate[] = Object.entries(rates).map(([currency, rate]) => {
    return {
      key: currency,
      currency,
      rate,
    }
  })
  return (
    <Select
      autoFocus
      showSearch
      defaultOpen={true}
      size="large"
      defaultValue={selected}
      style={{ width: '100%' }}
      onChange={handler}
    >
      {data.map(
        ({ key, currency, rate }): JSX.Element => (
          <Option key={key} value={currency}>
            <span style={{ fontWeight: 'bold' }}>{currency}:</span>{' '}
            <span style={{ color: 'green' }}>{rate}</span>
          </Option>
        )
      )}
    </Select>
  )
}
