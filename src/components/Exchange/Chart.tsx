import React from 'react'
import { Tag } from 'antd'
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  ResponsiveContainer,
  Tooltip,
} from 'recharts'

import { useHistoricalRates } from '../../hooks/useHistorical'

interface IHistoricalDatum {
  key: string
  date: string
  rate: any
}

export const MinMax = ({ domain }: { domain: [any?, any?] }) => {
  let [min, max] = domain
  // Undo chart domain additions
  min = min + 0.1
  max = max - 0.1

  return (
    <>
      <Tag color="red">Minimum: {min}</Tag>
      <Tag color="green">Maximum: {max}</Tag>
    </>
  )
}

export const Chart = ({
  symbol,
  date,
}: {
  symbol: string
  date: string
}): JSX.Element | null => {
  const { historical } = useHistoricalRates(date, symbol)
  let datasource: IHistoricalDatum[] = []
  let domain: [any?, any?] = []

  if (historical) {
    datasource = historical
      .map((data, i) => {
        const [date, rate] = Object.entries(data)[0]
        return { key: String(i), date, rate }
      })
      .reverse()

    const rates = datasource.map(({ rate }) => rate)

    domain = [
      Math.min(...Object.values(rates)) - 0.1,
      Math.max(...Object.values(rates)) + 0.1,
    ]
  }

  return historical?.length ? (
    <>
      <ResponsiveContainer width="100%" height={400}>
        <LineChart data={datasource} style={{ marginLeft: '-20px' }}>
          <Line
            type="monotone"
            dataKey="rate"
            stroke="#8884d8"
            strokeWidth={2}
          />
          <CartesianGrid strokeDasharray="3 3" stroke="#ccc" />
          <XAxis dataKey="date" />
          <YAxis
            type="number"
            domain={domain}
            tickFormatter={(t) => String(Math.round(t * 100) / 100)}
          />
          <Tooltip />
        </LineChart>
      </ResponsiveContainer>
      <MinMax domain={domain} />
    </>
  ) : null
}
