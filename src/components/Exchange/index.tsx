import React, { useState } from 'react'
import { Row, Col } from 'antd'

import { Chart } from './Chart'
import { Selector } from './Selector'

export const Exchange = ({ payload }: any): JSX.Element => {
  const { rates, date } = payload
  const [selected, setSelected] = useState<string>()

  return (
    <>
      <h1 style={{ textAlign: 'center' }}>
        Select any currency to view its historical data (last 7 days)
      </h1>
      <Row>
        <Col span={6} offset={9}>
          <Selector rates={rates} selected={selected} handler={setSelected} />
        </Col>
      </Row>
      <Row>
        <Col span={20} offset={2}>
          {selected ? <Chart symbol={selected} date={date} /> : null}
        </Col>
      </Row>
    </>
  )
}
