import React from 'react'
import { Alert } from 'antd'

import CONSTANTS from '../constants.json'

export const Error = ({ e }: any): JSX.Element => {
  const { error } = e
  return (
    <>
      <Alert
        message={CONSTANTS.SOMETHING_WENT_WRONG}
        type="error"
        description={
          <>
            <h3>Error code:</h3>
            <p> {error?.code}</p>
            <h3>Error message:</h3>
            <p> {error?.message}</p>
          </>
        }
      />
      <br />
      {error.code === CONSTANTS.RESTRICTED_CURRENCY_ACCESS_ERROR_MESSAGE ? (
        <Alert type="info" message={CONSTANTS.RESTRICTED_CURRENCY_INFO} />
      ) : null}
    </>
  )
}
