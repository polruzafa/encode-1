import React from 'react'
import { Layout, PageHeader, Spin } from 'antd'
import { useSymbols } from '../hooks/useSymbols'
import { useLatestRates } from '../hooks/useLatest'

import { PreferredSelector } from './PreferredSelector'
import { Exchange } from './Exchange'
import { Error } from './Error'

const { Header, Content } = Layout

export const Container = (): JSX.Element => {
  const { symbols } = useSymbols()
  const { rates, info } = useLatestRates()
  const { loading, error } = info
  let currencies: string[] = []

  if (symbols) {
    currencies = Object.keys(symbols)
  }

  return (
    <Layout>
      <Header>
        <PageHeader
          title="💸🚀 Exchange Rates App"
          extra={[<PreferredSelector currencies={currencies} />]}
        />
      </Header>
      <Content style={{ padding: '50px', textAlign: 'center' }}>
        {loading ? (
          <Spin />
        ) : rates ? (
          <Exchange payload={rates} />
        ) : error ? (
          <Error e={error} />
        ) : null}
      </Content>
    </Layout>
  )
}
