export interface IResult {
  payload?: any | null
  loading?: boolean
  error?: any | null
}
