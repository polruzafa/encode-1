import React from 'react'
import { Container } from './components/Container'
import { CurrencyProvider } from './hooks/useCurrency'

import './App.css'

function App() {
  return (
    <CurrencyProvider>
      <Container />
    </CurrencyProvider>
  )
}

export default App
