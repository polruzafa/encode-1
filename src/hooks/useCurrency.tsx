import React, { useContext, useState } from 'react'
import CONSTANTS from '../constants.json'

interface ICurrencyContext {
  currency?: string
  setCurrency?: (value: string) => void
}

const CurrencyContext = React.createContext<ICurrencyContext>({})

export const CurrencyProvider = ({ children }: any): JSX.Element => {
  const [data, setData] = useState<string>(
    localStorage.getItem(CONSTANTS.DATA_KEY) || CONSTANTS.DEFAULT_CURRENCY
  )

  const saveData = (value: string) => {
    setData(value)
    localStorage.setItem(CONSTANTS.DATA_KEY, value)
  }

  return (
    <CurrencyContext.Provider value={{ currency: data, setCurrency: saveData }}>
      {children}
    </CurrencyContext.Provider>
  )
}

export const useCurrency = (): any => {
  const { currency, setCurrency } = useContext(CurrencyContext)
  return { currency, setCurrency }
}
