export function fakeFetch(
  url: string,
  time: number,
  payload: any
): Promise<Object> {
  console.log(`Resolving ${url} in ${time} seconds...`)
  return new Promise((resolve: any, reject: any) => {
    setTimeout(() => {
      resolve(payload)
    }, time * 1000)
  })
}
