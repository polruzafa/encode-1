import { useEffect, useState } from 'react'
import { subDays, format } from 'date-fns'
import { useCurrency } from './useCurrency'
import { IResult } from '../types/result'
import CONSTANTS from '../constants.json'

const calculate7days = (date: string): string[] => {
  const dates: string[] = []
  for (let i = 1; i < 8; i += 1) {
    dates.push(format(subDays(new Date(date), i), 'yyyy-MM-dd'))
  }
  return dates
}

interface IHistorical {
  historical: any[]
  info?: any
}

export const useHistoricalRates = (
  today: string,
  symbol: string | undefined
): IHistorical => {
  const { REACT_APP_API_KEY } = process.env
  const {
    API_BASE_URL,
    API_SYMBOLS_PARAM,
    API_BASE_PARAM,
    API_ACCESS_KEY_PARAM,
  } = CONSTANTS
  const { currency } = useCurrency()
  const [result, setResult] = useState<IResult>({
    payload: null,
    loading: true,
    error: null,
  })

  useEffect(() => {
    async function fetchHistorical() {
      try {
        const dates = calculate7days(today)
        const urls = dates.map((date) => {
          const url = new URL(`${API_BASE_URL}/${date}`)
          url.searchParams.append(API_BASE_PARAM, currency)
          url.searchParams.append(API_SYMBOLS_PARAM, symbol!)
          url.searchParams.append(API_ACCESS_KEY_PARAM, REACT_APP_API_KEY!)
          return url.toString()
        })

        // Mind the quota!
        const responses = await Promise.all(urls.map((url, i) => fetch(url)))
        const jsons = await Promise.all(
          responses.map((response) => response.json())
        )

        const processed = jsons.flatMap(({ rates }, i) => ({
          [dates[i]]: rates[symbol!],
        }))
        setResult({
          payload: processed,
          loading: false,
          error: null,
        })
      } catch (e) {
        setResult({ payload: null, loading: false, error: e })
      }
    }
    if (currency && symbol) {
      fetchHistorical()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currency, symbol])

  return {
    historical: result.payload,
    info: { loading: result.loading, error: result.error },
  }
}
