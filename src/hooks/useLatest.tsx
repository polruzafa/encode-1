import { useEffect, useState } from 'react'
import { useCurrency } from './useCurrency'
import { IResult } from '../types/result'
import CONSTANTS from '../constants.json'

interface ILatest {
  rates: any[]
  info?: any
}

export const useLatestRates = (): ILatest => {
  const { REACT_APP_API_KEY } = process.env
  const {
    API_BASE_URL,
    API_LATEST_ENDPOINT,
    API_BASE_PARAM,
    API_ACCESS_KEY_PARAM,
  } = CONSTANTS
  const { currency } = useCurrency()
  const [result, setResult] = useState<IResult>({
    payload: null,
    loading: false,
    error: null,
  })

  useEffect(() => {
    async function fetchLatest() {
      try {
        const url = new URL(`${API_BASE_URL}${API_LATEST_ENDPOINT}`)
        url.searchParams.append(API_BASE_PARAM, currency)
        // non-null trust operator `!`
        url.searchParams.append(API_ACCESS_KEY_PARAM, REACT_APP_API_KEY!)

        setResult({ payload: null, loading: true, error: null })
        // Mind the quota!
        const response = await fetch(url.toString())
        const json = await response.json()
        // Bad request && Server Errors are not Exceptions
        if (!response.ok) {
          setResult({ payload: null, loading: false, error: json })
        } else {
          setResult({
            payload: json,
            loading: false,
            error: null,
          })
        }
      } catch (e) {
        console.error(e)
        setResult({ payload: null, loading: false, error: e })
      }
    }
    if (currency) {
      fetchLatest()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currency])

  return {
    rates: result.payload,
    info: { loading: result.loading, error: result.error },
  }
}
