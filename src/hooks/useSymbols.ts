import { useEffect, useState } from 'react'
import { IResult } from '../types/result'
import CONSTANTS from '../constants.json'

interface ISymbols {
  symbols: string[]
  info?: any
}

export const useSymbols = (): ISymbols => {
  const { REACT_APP_API_KEY } = process.env
  const { API_BASE_URL, API_SYMBOLS_ENDPOINT, API_ACCESS_KEY_PARAM } = CONSTANTS
  const [result, setResult] = useState<IResult>({
    payload: null,
    loading: false,
    error: null,
  })

  useEffect(() => {
    async function fetchSymbols() {
      try {
        const url = new URL(`${API_BASE_URL}${API_SYMBOLS_ENDPOINT}`)
        // non-null trust operator `!`
        url.searchParams.append(API_ACCESS_KEY_PARAM, REACT_APP_API_KEY!)

        setResult({ payload: null, loading: true, error: null })
        // Mind the quota!
        const response = await fetch(url.toString())
        const json = await response.json()
        // Bad request && Server Errors are not Exceptions
        if (!response.ok) {
          setResult({ payload: null, loading: false, error: json })
        } else {
          setResult({
            payload: json.symbols,
            loading: false,
            error: null,
          })
        }
      } catch (e) {
        console.error(e)
        setResult({ payload: null, loading: false, error: e })
      }
    }
    fetchSymbols()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return {
    symbols: result.payload,
    info: { loading: result.loading, error: result.error },
  }
}
